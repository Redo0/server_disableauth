Will not post to master server, so you have to use Connect To IP.
Assigns BLIDs to clients based on their IP address.
Requires PackageAnyFunction.
If your client is stuck in demo mode, add auth.blockland.us to your hosts file, and your key will work in offline mode.
