
$Pref::Server::ipsToBlid["127_0_0_1"] = getNumKeyId() + 0;

if(!$Pref::Server::ipsToBlidMax) $Pref::Server::ipsToBlidMax = 500000;

function ipToBlid(%address){
	%ip = getSubStr(%address, 0, strStr(%address, ":"));
	%ip = strReplace(%ip, ".", "_");
	
	echo("BLID from IP " @ %ip);
	
	if($Pref::Server::ipsToBlid[%ip]){
		%blid = $Pref::Server::ipsToBlid[%ip];
		echo("    Known BLID " @ %blid);
		return %blid;
		
	}else{
		%blid = $Pref::Server::ipsToBlidMax;
		$Pref::Server::ipsToBlidMax++;
		$Pref::Server::ipsToBlid[%ip] = %blid;
		
		echo("    New BLID " @ %blid);
		
		return %blid;
		
	}
}

package DisableAuth{
	function auth_Init_Server(){}
	function WebCom_PostServer(){}
	function GameConnection::authCheck(%client){
		%addr = %client.getAddress();
		%name = %client.netName;
		%blid = ipToBlid(%addr);
		
		echo("Addr = " @ %addr);
		echo("Name = " @ %name);
		echo("Blid = " @ %blid);
		
		%client.setPlayerName("au^timoamyo7zene", %name);
		%client.name = %name;
		%client.setBLID("au^timoamyo7zene", %blid);
		%client.bl_id = %blid;
		%client.setHasAuthedOnce(1);
		%client.startLoad();
		%client.killDupes();
	}
};
activatePackage(DisableAuth);

function rdar(){
	exec("./disableauth.cs");
}
